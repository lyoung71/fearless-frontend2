function createCard(name, location, description, pictureUrl, start, end) {
  return `
    <div className="card shadow mb-3">
      <img src="${pictureUrl}" className="card-img-top">
      <div className="card-body">
        <h5 className="card-title">${name}</h5>
        <h6 className="card-subtitle mb-2 text-muted">${location}</h6>
        <p className="card-text">${description}</p>
      </div>
      <div className="card-footer">
        <small className="text-muted">${start} - ${end}</small>
      </div>
    </div>
  `;
}

function createAlert(message, type) {
  return `
    <div className="alert alert-${type} alert-dismissible fade show" role="alert">
      ${message}
      <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  `;
}

function displayAlert(message, type) {
  const html = createAlert(message, type);
  const containerTag = document.querySelector('.container');
  const htmlElement = document.createElement('div');
  htmlElement.innerHTML = html;
  containerTag.append(htmlElement);
}

function handleResponseError(response) {
  if (!response.ok) {
    throw new Error(`An error has occurred: ${response.status} (${response.statusText})`);
  }
  return response;
}

function generatePlaceholders(amount) {
  for (let i = 0; i < amount; i++) {
    const html = `
      <div className="card" aria-hidden="true">
        <div className="card-body">
          <h5 className="card-title placeholder-glow">
            <span className="placeholder col-6"></span>
          </h5>
          <p className="card-text placeholder-glow">
            <span className="placeholder col-7"></span>
            <span className="placeholder col-4"></span>
            <span className="placeholder col-4"></span>
            <span className="placeholder col-6"></span>
            <span className="placeholder col-8"></span>
          </p>
        </div>
      </div>
    `;

    const columns = document.querySelectorAll('.col');
    columns[i % columns.length].innerHTML += html;
  }
}

window.addEventListener('DOMContentLoaded', async () => {
  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      throw new Error(`Error retrieving conferences: ${response.status} (${response.statusText})`);
    } else {
      const data = await response.json();

      generatePlaceholders(data.conferences.length);

      for (let i = 0; i < data.conferences.length; i++) {
        const columns = document.querySelectorAll('.col');


        const conference = data.conferences[i];
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);

        // Remove placeholders if they exists
        if (columns[i % columns.length].querySelector('.placeholder')) {
          columns[i % columns.length].innerHTML = '';
        }

        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const title = details.conference.name;
          const location = details.conference.location.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const startDate = new Date(details.conference.starts);
          const endDate = new Date(details.conference.ends);
          const formattedStart = startDate.toLocaleDateString();
          const formattedEnd = endDate.toLocaleDateString();
          const html = createCard(title, location, description, pictureUrl, formattedStart, formattedEnd);
          columns[i % columns.length].innerHTML += html;

        } else {
          throw new Error(`Error retrieving conference detail: ${detailResponse.status} (${detailResponse.statusText})`);
        }
      }
    }
  } catch (e) {
    displayAlert(e.message, 'danger');
  }
});
