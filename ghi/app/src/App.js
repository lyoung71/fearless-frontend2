import Nav from './Nav';
import AttendeesList from "./AttendeesList";
import LocationForm from "./LocationForm";
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import PresentationForm from './PresentationForm';
import { render } from "react-dom";
import MainPage from './MainPage';
import { BrowserRouter, Routes, Route, useParams } from "react-router-dom";


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
        <Routes>
            <Route path="/home" index element={<MainPage />} />
            <Route path="/locations/new" element={<LocationForm />} />
            <Route path="/conferences/new" element={<ConferenceForm />} />
            <Route path="/attendees/new" element={<AttendConferenceForm />} />
            <Route path="/attendees" element={<AttendeesList attendees={props.attendees} />} />
            <Route path="/presentations/new" element={<PresentationForm />} />
        </Routes>
    </BrowserRouter>
  );
}

export default App;
